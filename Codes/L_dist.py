import numpy as np
import simulationData
import stats
import cosmology
import SUT

r=simulationData.r
n_s1 = np.empty([len(r),25])
n_s2 = np.empty([len(r),25])
n_s3 = np.empty([len(r),25])

#### redshift zero spin distribution
for i in range(len(r)):
    spin1 = simulationData.L(0.0,r[i])
    n_s1[i,], sbin1 = np.histogram(spin1,bins=25,range=[-3,3],normed=True)

    spin2 = simulationData.L(0.5,r[i])
    n_s2[i,], sbin2 = np.histogram(spin2,bins=25,range=[-3,3],normed=True)

    spin3 = simulationData.L(-0.5,r[i])
    n_s3[i,], sbin3 = np.histogram(spin3,bins=25,range=[-3,3],normed=True)

n_s1,a = stats.avg_and_error_(n_s1,n_s1)
n_s2,a  = stats.avg_and_error_(n_s2,n_s2)
n_s3,a  = stats.avg_and_error_(n_s3,n_s3)
g = open('/media/swati/WoRKSPaCe/assembly-bias/ProcessedData/Spin/p_L.txt', 'w')
np.savetxt(g, np.transpose([sbin1[:25],n_s1,n_s2,n_s3]))

# ########## mass dependance
n_s1 = np.empty([len(r),25])
n_s2 = np.empty([len(r),25])
n_s3 = np.empty([len(r),25])

for i in range(len(r)):
    spin1 = simulationData.L(0.0,r[i])
    mass = np.array(simulationData.getData_f(23,0.0,r[i]))/SUT.h_til(0.0)
    ind = np.where((mass>=1e13)& (mass<= 2.5e13))
    spin1 = spin1[ind]
    n_s1[i,], sbin1 = np.histogram(spin1,bins=25,range=[-3,3],normed=True)

    spin2 = simulationData.L(0.5,r[i])
    mass = np.array(simulationData.getData_f(23, 0.5, r[i]))/SUT.h_til(0.5)
    ind = np.where((mass >= 1e13) & (mass <= 2.5e13))
    spin2 = spin2[ind]
    n_s2[i,], sbin1 = np.histogram(spin2,bins=25,range=[-3,3],normed=True)

    spin3 = simulationData.L(-0.5,r[i])
    mass = np.array(simulationData.getData_f(23, -0.5, r[i]))/SUT.h_til(-0.5)
    ind = np.where((mass >= 1e13) & (mass <= 2.5e13))
    spin3 = spin3[ind]
    n_s3[i,], sbin1 = np.histogram(spin3,bins=25,range=[-3,3],normed=True)

n_s1,a  = stats.avg_and_error_(n_s1,n_s1)
n_s2,a  = stats.avg_and_error_(n_s2,n_s2)
n_s3,a  = stats.avg_and_error_(n_s3,n_s3)
h = open('/media/swati/WoRKSPaCe/assembly-bias/ProcessedData/Spin/p_L_m1e13.txt', 'w')
np.savetxt(h, np.transpose([sbin1[:25],n_s1,n_s2,n_s3]))


n_s1 = np.empty([len(r),25])
n_s2 = np.empty([len(r),25])
n_s3 = np.empty([len(r),25])

for i in range(len(r)):
    spin1 = simulationData.L(0.0,r[i])
    mass = np.array(simulationData.getData_f(23,0.0,r[i]))/SUT.h_til(0.0)
    ind = np.where((mass>=7e13)& (mass< 1e14))
    spin1 = spin1[ind]
    n_s1[i,], sbin1 = np.histogram(spin1,bins=25,range=[-3,3],normed=True)

    spin2 = simulationData.L(0.5,r[i])
    mass = np.array(simulationData.getData_f(23, 0.5, r[i]))/SUT.h_til(0.5)
    ind = np.where((mass >= 7e13) & (mass <= 1e14))
    spin2 = spin2[ind]
    n_s2[i,], sbin1 = np.histogram(spin2,bins=25,range=[-3,3],normed=True)

    spin3 = simulationData.L(-0.5,r[i])
    mass = np.array(simulationData.getData_f(23, -0.5, r[i]))/SUT.h_til(-0.5)
    ind = np.where((mass >= 7e13) & (mass <= 1e14))
    spin3 = spin3[ind]
    n_s3[i,], sbin1 = np.histogram(spin3,bins=25,range=[-3,3],normed=True)

n_s1,a = stats.avg_and_error_(n_s1,n_s1)
n_s2,a = stats.avg_and_error_(n_s2,n_s2)
n_s3,a = stats.avg_and_error_(n_s3,n_s3)
f = open('/media/swati/WoRKSPaCe/assembly-bias/ProcessedData/Spin/p_L_m1e14.txt', 'w')
np.savetxt(f, np.transpose([sbin1[:25],n_s1,n_s2,n_s3]))