import numpy as np
import simulationData
import stats
import SUT

r=simulationData.r
# n_s1 = np.empty([len(r),20])
# n_s2 = np.empty([len(r),20])
# n_s3 = np.empty([len(r),20])
#
# #### redshift zero s distribution
# for i in range(len(r)):
#     s1 = simulationData.s(0.0,r[i])
#     n_s1[i,], sbin1 = np.histogram(s1,bins=20,range=[-5,3],normed=True)
#
#     s2 = simulationData.s(0.5,r[i])
#     n_s2[i,], sbin2 = np.histogram(s2,bins=20,range=[-5,3],normed=True)
#
#     s3 = simulationData.s(-0.5,r[i])
#     n_s3[i,], sbin3 = np.histogram(s3,bins=20,range=[-5,3],normed=True)
#
# n_s1,a = stats.avg_and_error_(n_s1,n_s1)
# n_s2,a  = stats.avg_and_error_(n_s2,n_s2)
# n_s3,a  = stats.avg_and_error_(n_s3,n_s3)
# g = open('/media/swati/WoRKSPaCe/assembly-bias/ProcessedData/Conc/p_s.txt', 'w')
# np.savetxt(g, np.transpose([sbin1[:20],n_s1,n_s2,n_s3]))

# ########## mass dependance
n_s1 = np.empty([len(r),20])
n_s2 = np.empty([len(r),20])
n_s3 = np.empty([len(r),20])

for i in range(len(r)):
    s1 = simulationData.s(0.0,r[i],1e13,2e13)
    n_s1[i,], sbin1 = np.histogram(s1,bins=20,range=[-5,3],normed=True)

    s2 = simulationData.s(0.4,r[i],1e13,2e13)
    n_s2[i,], sbin1 = np.histogram(s2,bins=20,range=[-5,3],normed=True)

    s3 = simulationData.s(-0.4,r[i],1e13,2e13)
    n_s3[i,], sbin1 = np.histogram(s3,bins=20,range=[-5,3],normed=True)

n_s1,a  = stats.avg_and_error_(n_s1,n_s1)
n_s2,a  = stats.avg_and_error_(n_s2,n_s2)
n_s3,a  = stats.avg_and_error_(n_s3,n_s3)
h = open('/media/swati/WoRKSPaCe/assembly-bias/ProcessedData/Conc/p_s_m1e13.txt', 'w')
np.savetxt(h, np.transpose([sbin1[:20],n_s1,n_s2,n_s3]))


n_s1 = np.empty([len(r),20])
n_s2 = np.empty([len(r),20])
n_s3 = np.empty([len(r),20])

for i in range(len(r)):
    s1 = simulationData.s(0.0,r[i],6e13,9e13)
    n_s1[i,], sbin1 = np.histogram(s1,bins=20,range=[-5,3],normed=True)

    s2 = simulationData.s(0.4,r[i],6e13,9e13)
    n_s2[i,], sbin1 = np.histogram(s2,bins=20,range=[-5,3],normed=True)

    s3 = simulationData.s(-0.4,r[i],6e13,9e13)
    n_s3[i,], sbin1 = np.histogram(s3,bins=20,range=[-5,3],normed=True)

n_s1,a = stats.avg_and_error_(n_s1,n_s1)
n_s2,a = stats.avg_and_error_(n_s2,n_s2)
n_s3,a = stats.avg_and_error_(n_s3,n_s3)
f = open('/media/swati/WoRKSPaCe/assembly-bias/ProcessedData/Conc/p_s_m1e14.txt', 'w')
np.savetxt(f, np.transpose([sbin1[:20],n_s1,n_s2,n_s3]))