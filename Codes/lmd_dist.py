import numpy as np
import simulationData
import stats
import cosmology
import SUT

r=simulationData.r
n_s1 = np.empty([len(r),40])
n_s2 = np.empty([len(r),40])
n_s3 = np.empty([len(r),40])

#### redshift zero spin distribution
for i in range(len(r)):
    spin1 = simulationData.lmd(0.0,r[i])
    # spin1 = spin1[spin1<=1.0]
    # spin1 = spin1[spin1 >= -3.0]
    n_s1[i,], sbin1 = np.histogram(spin1,bins=40,normed=True)
    spin2 = simulationData.lmd(0.5,r[i])
    # spin2 = spin2[spin2<=1.0]
    # spin2 = spin2[spin2 >= -3.0]
    n_s2[i,], sbin2 = np.histogram(spin2,bins=40,normed=True)
    spin3 = simulationData.lmd(-0.5,r[i])
    # spin3 = spin3[spin3<=1.0]
    # spin3 = spin3[spin3 >= -3.0]
    n_s3[i,], sbin3 = np.histogram(spin3,bins=40,normed=True)
n_s1,a = stats.avg_and_error_(n_s1,n_s1)
n_s2,a = stats.avg_and_error_(n_s2,n_s2)
n_s3,a = stats.avg_and_error_(n_s3,n_s3)
g = open('/media/swati/WoRKSPaCe/A_BIAS/assembly-bias/ProcessedData/Spin/p_l.txt', 'w')
np.savetxt(g, np.transpose([sbin1[:40],n_s1,n_s2,n_s3]))

########## mass dependance
n_s1 = np.empty([len(r),40])
n_s2 = np.empty([len(r),40])
n_s3 = np.empty([len(r),40])

for i in range(len(r)):
    spin1 = simulationData.lmd(0.0,r[i])
    mass = np.array(simulationData.getData_f(23,0.0,r[i]))/SUT.h_til(0.0)
    ind = np.where((mass>=1e13)& (mass<= 2e13))
    spin1 = spin1[ind]
    # spin1 = spin1[spin1<=1.0]
    # spin1 = spin1[spin1 >= -3.0]
    n_s1[i,], sbin1 = np.histogram(spin1,bins=40,normed=True)
    spin2 = simulationData.lmd(0.5,r[i])
    mass = np.array(simulationData.getData_f(23, 0.5, r[i]))/SUT.h_til(0.5)
    ind = np.where((mass >= 1e13) & (mass <= 2e13))
    spin2 = spin2[ind]
    # spin2 = spin2[spin2<=1.0]
    # spin2 = spin2[spin2 >= -3.0]
    n_s2[i,], sbin1 = np.histogram(spin2,bins=40,normed=True)
    spin3 = simulationData.lmd(-0.5,r[i])
    mass = np.array(simulationData.getData_f(23, -0.5, r[i]))/SUT.h_til(-0.5)
    ind = np.where((mass >= 1e13) & (mass <= 2e13))
    spin3 = spin3[ind]
    # spin3 = spin3[spin3<=1.0]
    # spin3 = spin3[spin3 >= -3.0]
    n_s3[i,], sbin1 = np.histogram(spin3,bins=40,normed=True)
n_s1,a = stats.avg_and_error_(n_s1,n_s1)
n_s2,a = stats.avg_and_error_(n_s2,n_s2)
n_s3,a = stats.avg_and_error_(n_s3,n_s3)
h = open('/media/swati/WoRKSPaCe/A_BIAS/assembly-bias/ProcessedData/Spin/p_l_m1e13.dat', 'w')
np.savetxt(h, np.transpose([sbin1[:40],n_s1,n_s2,n_s3]))


n_s1 = np.empty([len(r),40])
n_s2 = np.empty([len(r),40])
n_s3 = np.empty([len(r),40])

for i in range(len(r)):
    spin1 = simulationData.lmd(0.0,r[i])
    mass = np.array(simulationData.getData_f(23,0.0,r[i]))/SUT.h_til(0.0)
    ind = np.where((mass>=1e14)& (mass<= 2e14))
    spin1 = spin1[ind]
    # spin1 = spin1[spin1<=1.0]
    # spin1 = spin1[spin1 >= -3.0]
    n_s1[i,], sbin1 = np.histogram(spin1,bins=40,normed=True)
    spin2 = simulationData.lmd(0.5,r[i])
    mass = np.array(simulationData.getData_f(23, 0.5, r[i]))/SUT.h_til(0.5)
    ind = np.where((mass >= 1e14) & (mass <= 2e14))
    spin2 = spin2[ind]
    # spin2 = spin2[spin2<=1.0]
    # spin2 = spin2[spin2 >= -3.0]
    n_s2[i,], sbin1 = np.histogram(spin2,bins=40,normed=True)
    spin3 = simulationData.lmd(-0.5,r[i])
    mass = np.array(simulationData.getData_f(23, -0.5, r[i]))/SUT.h_til(-0.5)
    ind = np.where((mass >= 1e14) & (mass <= 2e14))
    spin3 = spin3[ind]
    # spin3 = spin3[spin3<=1.0]
    # spin3 = spin3[spin3 >= -3.0]
    n_s3[i,], sbin1 = np.histogram(spin3,bins=40,normed=True)
n_s1,a = stats.avg_and_error_(n_s1,n_s1)
n_s2,a = stats.avg_and_error_(n_s2,n_s2)
n_s3,a = stats.avg_and_error_(n_s3,n_s3)
f = open('/media/swati/WoRKSPaCe/A_BIAS/assembly-bias/ProcessedData/Spin/p_l_m1e14.dat', 'w')
np.savetxt(f, np.transpose([sbin1[:40],n_s1,n_s2,n_s3]))
