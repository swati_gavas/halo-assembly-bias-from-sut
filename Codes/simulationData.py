import numpy as np
import stats
import scipy as sp
from scipy.optimize import curve_fit
import cosmology
import SUT

# constants------------------------------------------------------
# mass units Msun
# Length units Mpc
L_box = 428.5714286
Mpart = 2.2e+10
a2 = 400 * Mpart

r = ['r13', 'r14', 'r15']
# ~ delta_L = [-0.1, -0.07, -0.05, -0.02, -0.01, 0.0, 0.01, 0.02, 0.05, 0.07, 0.1]#test
delta_L = [-0.7, -0.5, -0.4, -0.3, -0.2, -0.1, -0.07, -0.05, -0.02, -0.01, 0.0, 0.01, 0.02, 0.05, 0.07, 0.1, 0.15, 0.2,
           0.25, 0.3, 0.35, 0.4, 0.5, 0.7]

D_title = ['ID', 'DescID', 'Mvir', 'Vmax', 'Vrms', 'Rvir', 'Rs', 'Np', 'X', 'Y', 'Z', 'VX', 'VY', 'VZ', 'JX', 'JY',
           'JZ', 'Spin', 'rs_klypin', 'Mvir_all', 'M200b', 'M200c', 'M141.72324886b', 'M200_b', 'Xoff', 'Voff',
           'spin_bullock', 'b_to_a', 'c_to_a', 'A[x]', 'A[y]', 'A[z]', 'b_to_a(141.72324886b)', 'c_to_a(141.72324886b)',
           'A[x](141.72324886b)', 'A[y](141.72324886b)', 'A[z](141.72324886b)', 'T_|U|', 'M_pe_Behroozi', 'M_pe_Diemer',
           'Halfmass_Radius', 'PID']


def getData_(k, delta, realisation):
    filename = D_title[k] + '_' + str(delta) + '_' + realisation
    f = open('/media/swati/WoRKSPaCe/assembly-bias/RawData/' + filename, 'r')
    data = np.load(f)
    return data

def getData_f(k, delta, realisation):
    data = getData_(k, delta, realisation)
    eta = getData_(37, delta, realisation)
    ind = np.where(np.logical_and(-2 * eta + 1 >= -0.5, -2 * eta + 1 <= 0.5))
    data = data[ind]
    return data

def c(delta, realisation):
    M200b = np.array(getData_(23, delta, realisation))/SUT.h_til(delta)
    Rs = np.array(getData_(6, delta, realisation)) / 1000.0/SUT.h_til(delta)
    R200b = (3.0 * M200b / (4 * np.pi * 0.276 * 200 * cosmology.rho_crit*0.7*0.7)) ** (1.0 / 3.0)
    c = R200b / Rs
    return c

def s(delta, realisation, mstart, mend):
    lnc0 = np.log(c(0.0, realisation))
    mass = getData_(23, 0.0, realisation) / 0.7
    ind = np.where(np.logical_and(mass >= mstart, mass < mend))[0]
    lnc0 = lnc0[ind]
    lnc0_bar = np.median(lnc0)
    sgm_0 = stats.percentage_scatter(68.3, lnc0, 'middle')
    # sgm_0 = np.sqrt(np.var(lnc0))
    var_in = 0.1
    epsi_term = (sgm_0) ** 2 / var_in

    lnc = np.log(c(delta, realisation))
    mass = getData_(23, delta, realisation) / SUT.h_til(delta)
    ind = np.where(np.logical_and(mass >= mstart, mass < mend))[0]
    lnc = lnc[ind]
    # sgm_0 = stats.percentage_scatter(68.3, lnc, 'middle')
    s = (lnc - lnc0_bar) / (sgm_0)   #
    s *= np.sqrt(epsi_term)

    return s


def lmd(delta, realisation):
    l0 = getData_f(17, delta, realisation)
    return np.log10(l0)


def L(delta, realisation):
    L0_bar = -1.37386
    sgm_0 = np.log(10) * 0.2219
    L = lmd(delta, realisation)
    L = (L - L0_bar) / sgm_0
    return L

def n_m_deltaL(delta, Mstart, Mend, nbin):

    M = np.empty(nbin)
    n_m_delta = np.empty([len(r),nbin])
    for i in range(len(r)):
        Mass = getData_(23, delta, r[i])
        Mass = Mass / SUT.h_til(delta)
        lgMa = np.log10(Mass)

        n_m_delta[i,], lgM_bin = np.histogram(lgMa, bins=nbin, range=(np.log10(Mstart), np.log10(Mend)), normed=False)

    for j in range(nbin):
        mm = lgMa[(lgMa<lgM_bin[j+1]) & (lgMa>lgM_bin[j])]
        M[j] = np.median(mm)
    M = np.exp(np.log(10)*M)
    M = np.exp(np.log(10)*(lgM_bin[:nbin] + (lgM_bin[2]-lgM_bin[1])/2.0))

    return M, n_m_delta

def n_m_s_deltaL(delta, Mstart, Mend,sstart,send,nbin):

    M = np.empty(nbin)
    n_m_delta = np.empty([len(r),nbin])
    for i in range(len(r)):
        Mass = getData_(23, delta, r[i])
        s1 = s(delta,r[i],Mstart,Mend)
        Mass = Mass / SUT.h_til(delta)
        Mass = Mass[Mass>=Mstart]
        Mass = Mass[Mass<Mend]
        lgMa = np.log10(Mass)
        ind = np.where(np.logical_and(s1>=sstart,s1<send))[0]
        lgMa = lgMa[ind]

        n_m_delta[i,], lgM_bin = np.histogram(lgMa, bins=nbin,normed=False)

    for j in range(nbin):
        mm = lgMa[(lgMa<lgM_bin[j+1]) & (lgMa>lgM_bin[j])]
        M[j] = np.median(mm)
    # M = np.exp(np.log(10)*M)
    M = np.exp(np.log(10)*(lgM_bin[:nbin] + (lgM_bin[2]-lgM_bin[1])/2.0))

    return M, n_m_delta


