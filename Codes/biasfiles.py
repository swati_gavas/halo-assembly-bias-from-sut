import numpy as np
import bias
import cosmology

purpose='bias_s'
nj=bias.nj
b1=np.empty(nj)
b1t=np.empty(nj)
b1r=np.empty(nj)
b2=np.empty(nj)
err1=np.empty(nj)
err2=np.empty(nj)
M=bias.M

for j in range(nj):
    b1[j],err1[j],b2[j],err2[j] = bias.Bias_simulation(j,purpose)
    b1t[j]= cosmology.Bias1_expt_Mbin_Tinker10(M[j]*0.7,0.204,200)
    b1r[j]=b1[j]/b1t[j]

if purpose=='bias':
    f=open('/media/swati/WoRKSPaCe/assembly-bias/ProcessedData/Conc/bias.txt','w')
if purpose=='bias_s':
    f=open('/media/swati/WoRKSPaCe/assembly-bias/ProcessedData/Conc/bias_suq.txt','w')

np.savetxt(f, np.transpose([M,b1,err1,b1t,b2,err2,b1r,err1]))



