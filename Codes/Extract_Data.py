import numpy as np

#data reference
#~----ID(0)----DescID(1)----Mvir(2)----Vmax(3)----Vrms(4)----Rvir(5)----Rs(6)----Np(7)----X(8)----Y(9)----Z(10)----VX(11)----VY(12)----VZ(13)----JX(14)----JY(15)----JZ(16)----Spin(17)----rs_klypin(18)----Mvir_all(19)----M200b(20)----M200c(21)----M141.72324886b(22)----M109.95214922b(23)----Xoff(24)----Voff(25)----spin_bullock(26)----b_to_a(27)----c_to_a(28)----A[x](29)----A[y](30)----A[z](31)----b_to_a(141.72324886b)(32)----c_to_a(141.72324886b)(33)----A[x](141.72324886b)(34)----A[y](141.72324886b)(35)----A[z](141.72324886b)(36)----T/|U|(37)----M_pe_Behroozi(38)----M_pe_Diemer(39)----Halfmass_Radius(40)----PID(41)

D_title = ['ID','DescID','Mvir','Vmax','Vrms','Rvir','Rs','Np','X','Y','Z','VX','VY','VZ','JX','JY','JZ','Spin','rs_klypin','Mvir_all','M200b','M200c','M141.72324886b','M200_b','Xoff','Voff','spin_bullock','b_to_a','c_to_a','A[x]','A[y]','A[z]','b_to_a(141.72324886b)','c_to_a(141.72324886b)','A[x](141.72324886b)','A[y](141.72324886b)','A[z](141.72324886b)','T/|U|','M_pe_Behroozi','M_pe_Diemer','Halfmass_Radius','PID']

delta_L = [-0.7, -0.5, -0.4, -0.3, -0.2, -0.1, -0.07, -0.05, -0.02, -0.01, 0.0, 0.01, 0.02, 0.05, 0.07, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.5, 0.7]

def Path(realisation,delta):
    PATH = '/media/swati/WoRKSPaCe/A_BIAS/simulations/su512/halos/delta' + str(delta) +            '/'+realisation+'/out_1.parents'
    return PATH

def getData_(k, delta, realisation,):
    PATH = Path(realisation, delta)
    f = open(PATH, 'r')
    print 'Reading data...',D_title[k],'...   delta = ', delta,'     realisation = ', realisation

    content = f.readlines()[16:]
    l = len(content)
    D = np.empty([42, l])

    i = 0
    for line in content:
        D[0, i], D[1, i], D[2, i], D[3, i], D[4, i], D[5, i], D[6, i], D[7, i], D[8, i], D[9, i], D[10, i], D[11, i], D[12, i], D[13, i], D[14, i], D[15, i], D[16, i], D[17, i], D[18, i], D[19, i], D[20, i], D[21, i], D[22, i], D[23, i], D[24, i], D[25, i], D[26, i], D[27, i], D[28, i], D[29, i], D[30, i], D[31, i], D[32, i], D[33, i], D[34, i], D[35, i], D[36, i], D[37, i], D[38, i], D[39, i], D[40, i], D[41, i] = line.split(' ')

        i += 1
    print 'reading done'
    print 'filtering data...'
    data = D[k,]
    print len(data)
    eta = D[37,]
    pid = D[41,]
    rm_ipid = np.where(pid != -1.0)[0]
    data = np.delete(data, rm_ipid, axis=0)
    eta = np.delete(eta, rm_ipid, axis=0)
    rm_ieta = np.where(eta >= 1.0)[0]
    data = np.delete(data, rm_ieta, axis=0)
    print len(data)
    print 'filtering done'
    return data

def saveData_(k,delta,realisation):

    filename = D_title[k] + '_'+str(delta)+'_'+realisation
    f = open('/media/swati/WoRKSPaCe/A_BIAS/assembly-bias/RawData'+filename, 'w')
    np.save(f,getData_(k,delta,realisation))
    return

print 'generate data...'
generate ='y'
while generate=='y':
    strr = raw_input('ENTER...data_')
    k = int(strr)
    for i in range(len(delta_L)):
        saveData_(k,delta_L[i],'r13')
        saveData_(k,delta_L[i], 'r14')
        saveData_(k,delta_L[i], 'r15')
    print 'generated'
    generate = raw_input('generate another [y/n]? ')
