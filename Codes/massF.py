import numpy as np
import simulationData
import stats
import cosmology

mstart = 6e11 /0.7
mend = 2e15 /0.7
nbin=17
dlgm=(np.log10(mend)-np.log10(mstart))/17.0

M, n_m = simulationData.n_m_deltaL(0.0,mstart,mend,nbin)
n_m, err = stats.avg_and_error(n_m,n_m)
n_m *= 0.7**-3
M *= 0.7

massFn = n_m/simulationData.L_box**3 /np.log(10)/dlgm
massFr = massFn*M /cosmology.rho_crit/0.276

massFnT =np.empty(len(M))
massFrT =np.empty(len(M))
for i in range(len(M)):
    massFnT[i]= cosmology.mass_function_tinker08(M[i],200)
    massFrT[i]= cosmology.mass_fraction_tinker08(M[i],200)

f = open('/media/swati/WoRKSPaCe/assembly-bias/ProcessedData/Mass/massFn.txt', 'w')
g = open('/media/swati/WoRKSPaCe/assembly-bias/ProcessedData/Mass/massFr.txt', 'w')
np.savetxt(f, np.transpose([M,massFn,0.7**-3 *err/dlgm/simulationData.L_box**3 /np.log(10),massFnT]))
np.savetxt(g, np.transpose([M,massFr,0.7**-3 *err/dlgm/simulationData.L_box**3 /np.log(10) *M/cosmology.rho_crit/0.276,massFrT]))
