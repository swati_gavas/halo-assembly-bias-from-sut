import numpy as np

def percentage_scatter(percent, array, end):

    """
    It gives value in a array, which separates array such that number of elements from the given end till the value makes given percentage of total arrya
    :param percent: percentage value (0-100)
    :param array: distribution array
    :param end: 'lower'/'upper'-starts counting from minimum/maximum value in array
                'middle'-starts counting from value having maximum multiplicity
    :return: value
    """

    l = len(array)
    nbin = 12000#l/4
    mx = max(array)
    mn = min(array)
    binsize = (mx-mn)/nbin
    nm, bins = np.histogram(array,nbin,range=(mn,mx),normed=False)
    ind = np.where(nm==max(nm))[0][0]
    bins = bins + binsize/2.0
    frac = percent/100.0
    if (end=='lower'):
        p=0
        i=-1
        while (abs(p/float(l) - frac)>1e-4):
            i+=1
            sample = np.where(array<=bins[i])[0]
            p = len(sample)
        return bins[i]
    elif (end=='upper'):
        p=0
        i=len(bins)
        while (abs(p/float(l) - frac)>1e-4):
            i-=1
            sample = np.where(array>=bins[i])[0]
            p = len(sample)
        return bins[i]
    else:
        p = 0
        lim1=bins[ind]
        lim2 = lim1
        while (abs(p / float(l) - frac) > 1e-4):
            print p / float(l)
            array1 = array[array<=lim2]
            array1 = array1[array1 >lim1]
            lim1 = lim1-0.0001
            lim2 = lim2+0.0001
            p = len(array1)
        return lim1,lim2,abs(lim2-lim1)/2.0
import simulationData
# array = simulationData.lmd(0.0,'r13')
# array = np.log(array)
# print percentage_scatter(68.3,array,'middle'),np.median(array)

def avg_and_error_(array,w):

    """
    :param array: 2D array, row repersents diferent random variables and column represents different values for single random variavle
    :return: avg, error: avg is weighted of the column values and error for each mean value
    """
    p = np.size(array, 0) #row index
    q = np.size(array, 1) #column index
    col_var = np.empty(q)
    col_bar = np.empty(q)
    serr = np.empty(q)
    n=p
    for j in range(q):
        for i in range(n-1):
            if array[i,j]==np.inf:
                array = np.delete(array,[i,],axis=0)
                w = np.delete(w,[i,],axis=0)
                n=n-1
                break
        if (w[0,j]+w[1,j]+w[2,j]==0.0):
            w[0,j]=1e-15
            w[1, j] = 1e-15
            w[2, j] = 1e-15


    col_bar = np.average(array,axis=0,weights=w)
    for j in range(q):
        col_var = np.average((array-col_bar[j])**2, axis=0,weights=w)
        serr[j] = np.sqrt(col_var[j]/n)

    return col_bar, serr
