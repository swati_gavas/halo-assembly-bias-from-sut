import numpy as np
import scipy as sp
import scipy.integrate as integrate
from scipy.optimize import fsolve
import cosmology

O_m		= cosmology.O_m
O_l		= cosmology.O_l
h		= cosmology.h


def H(a):
	return np.sqrt(O_m/a**3 +O_l)

def integ_growth_fact(a):
	return  1.0/(a*H(a))**3

def growth_fact(a):
	integ =sp.integrate.quad(integ_growth_fact, 0, a)
	#~ D1 = 5.0/2.0* O_m/(O_m**(4.0/7.0) - O_l + (1+O_m/2.0)*(1+O_l/70.0))
	return 5.0*O_m*integ[0] /2.0

def d_H(delta):
	
	D1 = growth_fact(1)
	k_til_H02 = 5.0* O_m * delta /3.0/D1
	return (1 - k_til_H02)**0.5 - 1

def Om_til(delta):

	O_m_til = O_m * (1 + d_H(delta))**-2
	return O_m_til

def Ol_til(delta):

	O_l_til = O_l * (1 + d_H(delta))**-2
	return O_l_til

def Ok_til(delta):

	O_k_til = 1.0 - (1 + d_H(delta))**-2
	return O_k_til

def h_til(delta):

	h_til_sqr = O_m * h**2 /Om_til(delta)
	h_til = np.sqrt(h_til_sqr)
	return h_til

def H_til(a_til, delta):

	return (1+d_H(delta))*np.sqrt(Om_til(delta)/a_til**3 + Ok_til(delta)/a_til**2 + Ol_til(delta))

def integ_tout(a_til, delta):
	return  1.0/a_til/H_til(a_til, delta)

def tout(a_til, delta):
	return sp.integrate.quad(integ_tout, 0, a_til, args=(delta))[0]

def a_til(delta):

	def func(a_til):
		return tout(a_til, delta) - tout(1,0.0)

	ans, = fsolve(func,1)
	return ans

def SO_cust(delta):

	return 200*a_til(delta)**3