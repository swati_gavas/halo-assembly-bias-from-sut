import numpy as np
import simulationData
import stats

mstart = 8.6e12
mend = 6.6e14
sstart=stats.percentage_scatter(25,simulationData.s(0.0,'r15',mstart,mend),'upper')
send=np.inf
# sstart=-np.inf
# send=stats.percentage_scatter(25,simulationData.s(0.0,'r15',mstart,mend),'lower')


nbin=9
delta_L = simulationData.delta_L
r= simulationData.r
ni=len(r)
nj=nbin
nk=len(delta_L)
M,dn00=simulationData.n_m_deltaL(0.0,mstart,mend,nbin)

def dh_i_m_dl_(purpose):

    dh_i_m_dl = np.empty([ni,nj,nk])
    w = np.empty([ni, nj, nk])

    if purpose=='bias':
        M, dn_00 = simulationData.n_m_deltaL(0.0,mstart,mend,nbin)
    if purpose=='bias_s':
        M, dn_00 = simulationData.n_m_s_deltaL(0.0, mstart, mend,sstart,send, nbin)

    for k in range( len(delta_L)):

        if purpose=='bias':
            M, dn_delta = simulationData.n_m_deltaL(delta_L[k],mstart,mend,nbin)
        if purpose=='bias_s':
            print purpose
            M, dn_delta = simulationData.n_m_s_deltaL(delta_L[k], mstart, mend, sstart, send, nbin)

        dh = dn_delta * 1.0 / dn_00 - 1.0
        for j in range(nj):
            for i in range(ni):
                dh_i_m_dl[i,j,k]=dh[i,j]
                w[i,j,k] = dn_delta[i,j]
    return dh_i_m_dl,w

def dh_dl_(j,perpose):

    dh_i_m_dl,w= dh_i_m_dl_(perpose)
    dh_dl = np.empty([ni,nk])
    w1 = np.empty([ni, nk])

    for i in range(ni):
        for k in range(nk):
            dh_dl[i,k]=dh_i_m_dl[i,j,k]
            w1[i,k]= w[i,j,k]
    dh_dl,err = stats.avg_and_error_(dh_dl,w1)

    return dh_dl,err

import scipy as sp
from scipy.optimize import curve_fit as optimize

def fit_func(x, b1, b2, b3, b4):
    y = b1 * x + (b2 * x * x) / 2.0 + (b3 * x * x * x) / 6.0 + (b4 * x * x * x * x) / 24.0
    return y

def dhl_polyfit(j, purpose):

    dh_dl,err1 = dh_dl_(j, purpose)
    # P, covar = sp.optimize.curve_fit(fit_func, delta_L, dh_dl,sigma=1/err1)
    print err1
    P,covar =  np.polyfit(delta_L,dh_dl,4,w=np.power(err1 ,1),cov=True,full=False)
    P=np.flip(P,0)

    return P,np.flip(np.diagonal(covar)/3.0,0)

def Bias_simulation(j, purpose):

    P, err = dhl_polyfit(j, purpose)
    err /= np.sqrt(ni)
    b1 = P[1] + 1
    b2 = P[2] + (8 / 21) * P[0]
    print 'Bias ', j + 1
    return b1,err[1], b2,err[2]



