import scipy as sp
import scipy.integrate as integrate
import numpy as np

#constants at Z=0 -----------------------------------------------------------------
##mass unit Msun
##length unit Mpc
rho_crit 	= 2.774848e+11		# Msun h^2/Mpc^3
delta_coll	= 1.686
O_l			= 0.724000
O_m			= 0.276000
O_b			= 0.045000
O_c			= O_m - O_b
h 			= 0.7
n_s			= 0.961				# primordial spectral index
Tcmb		= 2.7255			# K
f_bm		= O_b /O_m
f_cm		= O_c /O_m
e			= 2.71828182846
c			= 299792458 		# m/s
H_0			= 100*h 			# (km/sec)/Mpc
sigma_8		= 0.811
Dp			= 5.0/2.0 * O_m/(O_m**(4.0/7.0) - O_l + (1+O_m/2.0)*(1+O_l/70.0))

def LagrangianR(M):

	"""The Lagrangian radius R (Mpc/h) of halo of mass M (Msun/h).
	That is the volume that encloses the halo's mass given the mean density of the universe at z = 0"""
	
	return (3.0 * M / 4.0 / np.pi / (O_m*rho_crit))**(1.0 / 3.0)

def transfer_function(k):
	"""
	The transfer function according to Eisenstein & Hu 1998.
	k - The wavenumber k (h/Mpc)
	Tk- The transfer function; has the same dimensions as k.
	"""
	
	theta_27 = Tcmb/2.7
	#The transition from a radiation-dominated universe to a matter-dominated one occurs roughly at
	z_eq = 2.50e4 * O_m*h*h /(theta_27**4)
	#scale of the particle horizon at zeq
	k_eq = 7.46e-2 * O_m*h*h /(theta_27*theta_27) # Mpc^{-1}
	# Convert kh from h/Mpc to 1/Mpc
	kh = k * h
	#drag epoch
	b_1 = 0.313 * np.power(O_m*h*h,-0.419) * (1.0 + 0.607*np.power(O_m*h*h,0.674))
	b_2 = 0.238 * np.power(O_m*h*h, 0.223)
	z_d = 1291.0 * (O_m*h*h)**0.251 / (1.0 + 0.659 * (O_m*h*h)**0.828) * (1.0 + b_1*(O_m*h*h)**b_2)

	#R - the ratio of the baryon to photon momentum density. sound speed c_s=(sqrt(3+3R))^-1
	R_d = 31.5 * O_b*h*h /theta_27**4 / (z_d/1e3)
	R_eq = 31.5 * O_b*h*h /theta_27**4 /(z_eq/1e3)
	
	#sound horizon at drag epoch
	s = (2.0/3.0/k_eq) * np.sqrt(6.0/R_eq) * np.log((np.sqrt(1.0 + R_d) + np.sqrt(R_d + R_eq))/(1.0 + np.sqrt(R_eq)))
	
	#silk damping
	k_silk = 1.6 * np.power(O_b*h*h,0.52) * np.power(O_m*h*h,0.73)*(1.0 + np.power(10.4*O_m*h*h,-0.95))
	q = kh /13.41 /k_eq
	
	#CDM transfer function
	a_1 = np.power(46.9*O_m*h*h,0.670) * (1.0 + np.power(32.1*O_m*h*h,-0.532))
	a_2 = np.power(12.0*O_m*h*h,0.424) * (1.0 + np.power(45.0*O_m*h*h,-0.582))
	alpha_c = np.power(a_1,-1.0*f_bm) * np.power(a_2,-1.0*f_bm**3)
	b_1 = 0.944 /(1.0 + np.power(458.0*O_m*h*h,-0.708))
	b_2 = np.power(0.395*O_m*h*h,-0.0266)
	beta_c = 1.0/(1.0 + b_1*(np.power(f_cm,b_2) - 1.0))
	
	f = 1.0/(1.0 + (kh*s/5.4)**4)
	C = 14.2/alpha_c + 386.0/(1.0 + 69.9*np.power(q,1.08))
	C_1 = 14.2 + 386.0/(1.0 + 69.9*np.power(q,1.08))
	
	T_tilde = np.log(e + 1.8*beta_c*q)/(np.log(e + 1.8*beta_c*q) + C*q*q)
	T_tilde_1 = np.log(e + 1.8*beta_c*q)/(np.log(e + 1.8*beta_c*q) + C_1*q*q)    
	T_c = f*T_tilde_1 + (1.0 - f)*T_tilde

	#Baryon transfer function
	y = (1.0 + z_eq)/(1.0 + z_d)
	Gy = y*(-6.0*np.sqrt(1.0 + y) + (2.0 + 3.0*y)*np.log((np.sqrt(1.0 + y) + 1.0)/(np.sqrt(1.0 + y) - 1.0)))
	alpha_b = 2.07 * k_eq*s* np.power(1.0 + R_d,-3.0/4.0) * Gy
	beta_b  = 0.5 + f_bm + (3.0 - 2.0*f_bm)*np.sqrt(((17.2*O_m*h*h)**2) + 1.0)
	beta_node = 8.41*np.power(O_m*h*h,0.435)
	s_tilde = s/np.power(1.0 + (beta_node/kh/s)**3,1.0/3.0)
	T_tilde_11 = np.log(e + 1.8*q)/(np.log(e + 1.8*q) + C_1*q*q)    
	T_b = (T_tilde_11/(1.0 + (kh*s/5.2)**2) + alpha_b/(1.0 + (beta_b/kh/s)**3) /np.exp(np.power(kh/k_silk,1.4)))*np.sin(kh*s_tilde)/(kh*s_tilde)
	#total transfer function
	T_k = f_bm*T_b + f_cm*T_c
	return T_k

def tophat_filt(k, R):
	"""
	The tophat filter function for the variance in Fourier space. 
	This function is dimensionless, the input units are k in h/Mpc and R in Mpc/h.
	"""
	x = k * R
	if x < 1E-3:
		return 1.0
	else:
		return 3.0 / x**3 * (np.sin(x) - x * np.cos(x))

def MatterPowerSpectrum_Unorm(k):
	
	T = transfer_function(k)
	pk = k**n_s * T**2
	pk *= Dp**2
	return pk
	
def sigma_Unorm(R):
	
	#~ n_tilde = n_s -1
	#~ delta_H = 1.94e-5 * np.power(O_m, -0.785 - 0.05* np.log(O_m)) * np.exp(-0.95*n_tilde - 0.169*n_tilde**2)
	
	def integrand(k):
		pk =MatterPowerSpectrum_Unorm(k)
		del2_k = k**3/2/np.pi**2 * pk
		#~ del2_k =  (c/H_0 /1e3 *k)**(3+n_s) * (delta_H**2) * transfer_function(k)**2
		return del2_k * tophat_filt(k,R) * tophat_filt(k,R) /k

	AB = sp.integrate.quad(integrand, 0, np.inf)
	sigma_sqr = AB[0]
	return np.sqrt(sigma_sqr)

def MatterPowerSpectrum(k):

	"""
	The matter power spectrum at a scale k.
	k- The wavenumber k (h/Mpc), where :math:`10^{-20} < k < 10^{20}`
	Pk: The matter power spectrum has same dimensions as k.
	"""
	
	pk = MatterPowerSpectrum_Unorm(k)
	norm = (sigma_8/sigma_Unorm(8))**2
	pk *= norm
	return pk

def sigma(R):
	"""
	The rms variance of the linear density field on a scale R
	R- The radius of the filter in Mpc/h
	sigma- The rms variance; has the same dimensions as R.
	"""

	return sigma_8/sigma_Unorm(8) * sigma_Unorm(R)

def peak_height(M):
	"""
	Peak height nu of given a halo mass.
	M- Halo mass in Msun/h
	peak height; has the same dimensions as M.
	"""
	R = LagrangianR(M)
	sigma_m = sigma(R)
	nu = delta_coll/sigma_m
	return nu

def mass_fraction_tinker08(M, Del):

	# input mass in Msun/h and output function unitless
	lgM = np.log10(M)
	dlgM = np.log10(1e-10*M)
	R = LagrangianR(M)
	M_m = np.exp((lgM-(dlgM/2.0))*np.log(10))
	M_p = np.exp((lgM+(dlgM/2.0))*np.log(10))
	R_m = LagrangianR(M_m)
	R_p = LagrangianR(M_p)

	sgm = sigma(R)

	lgDel = np.log10(Del)
	if Del < 1600:
		A = 0.1*lgDel -0.05
	else:
		A = 0.26
		
	a = 1.43 + (lgDel - 2.3)**1.5
	b = 1.0 + (lgDel - 1.6)**(-1.5)
	c = 1.2 + (-lgDel + 2.35)**1.6
	f_sgm = A * ((sgm/b)**(-1*a) + 1) * e**(-1*c /sgm**2)

	dlnSgm_inv = np.log(sigma(R_m)/sigma(R_p))
	mass_fr = f_sgm * dlnSgm_inv /dlgM /np.log(10)
	
	print 'mass_f#_T08', M
	return mass_fr

def mass_function_tinker08(M, Del):

	# input mass in Msun/h and output function in (h/Mpc)^3
	mass_fr = mass_fraction_tinker08(M, Del)
	return mass_fr * rho_crit*O_m / M

def Bias1_tinker10(nu, Del):

	# input peak height corresponding to mass in Msun/h
	y = np.log10(Del)
	A = 1.0 + 0.24 * y * np.exp(-1.0 * (4.0 / y)**4)
	a = 0.44 * y - 0.88
	B = 0.183
	b = 1.5
	C = 0.019 + 0.107 * y + 0.19 * np.exp(-1.0 * (4.0 / y)**4)
	c = 2.4
	
	bias = 1.0 - A * nu**a / (nu**a + delta_coll**a) + B * nu**b + C * nu**c
	return bias

def Bias1_expt_Mbin_Tinker10(M_center, lgBinSize, Del):

	#M_center in units of Msun/h
	lgM_c = np.log10(M_center)
	lgM_min = lgM_c - lgBinSize/2.0
	lgM_max = lgM_c + lgBinSize/2.0
	lgMass = np.array(np.linspace(lgM_min, lgM_max, 10))
	Mass = np.exp(lgMass*np.log(10))
	integ1 = np.empty(len(Mass))
	integ2 = np.empty(len(Mass))

	i=0
	for data in Mass:
		
		nu = peak_height(Mass[i])
		b1 = Bias1_tinker10(nu, Del)
		massFunc = mass_function_tinker08(Mass[i], Del)
		integ1[i] = b1*massFunc
		integ2[i] = massFunc
		print 'expectation value of bias progress', i+1
		i+=1

	Num = sp.integrate.simps(integ1,Mass)
	Dino =sp.integrate.simps(integ2,Mass)
	
	return Num/Dino

def Bias2_L16(b1):
	
	return 0.412 - 2.143*b1 + 0.929*b1**2 + 0.008*b1**3
