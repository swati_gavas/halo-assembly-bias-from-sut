CODES

Main Files
1) cosmology.py - Contains all theoretical functions
2) SUT.py - Contains all functions related to Seperate Universe Technique
3) simulationData.py - Conains functions which work on data obtained from simulation
4) bias.py- Script to calculate bias from simulation with respect to mass and concentration

Extra
1) Extract_Data.py - Script to ectarct required data from simulation output files
2) massF.py - Script to obtain mass function and mass fraction of halos
3) s_dist- Script to obtain standard concentration disribution
4) lmd_dist.py - Script to obatin spin distribituin function from simulation 
5) L_dist.py - Script to obtain standard spin distribution3) stats.py - Conatins function to get average and errors in the data
6) biasfiles.py - Script to obtain bias data for ploting
7) stats.py - Contains statistic related functions




RESULTS

1) Mfunction.pdf - Mass function and mass fraction of halos
2) Bias.pdf - Bias(b1) with respect to mass
3) b1vsb2.pdf - Bias b1 vs bias b2 with respect to mass
4) s_dist.png - standard concentration distribution
5) b1vsnuu.png - Bias(b1) with respect to mass and standard concentration
  
